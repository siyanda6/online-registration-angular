import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Form} from './form';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL = 'http://localhost:8081/online-registration/api/form';

  constructor(private httpClient: HttpClient) { }

  public get() {
    return this.httpClient.get(this.SERVER_URL);
  }

  submit(input): Observable<Form> {
    return this.httpClient.post<Form>(this.SERVER_URL, input);
  }
}
