import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Form } from '../form';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-all-forms',
  templateUrl: './form-entry.component.html',
  styleUrls: ['./form-entry.component.css']
})
export class FormEntryComponent implements OnInit {

  title = 'Online Registration Form';
  formGroup;

  submitted = false;
  error = false;
  successful = false;
  errorMsg = '';

  model = new Form('', '', '');

  constructor(private formBuilder: FormBuilder, private apiService: ApiService) {
    this.formGroup = this.formBuilder.group({
      fullName: '',
      idNumber: '',
      telephoneNumber: ''
    }); }

  ngOnInit() {
  }

  onSubmit() {

    this.apiService.submit(this.model).subscribe(success => {
      this.submitted = true;
      this.error = false;
      this.successful = true;
      console.log(success);
    }, error => { // second parameter is to listen for error
      console.log(error);
      this.submitted = true;
      this.error = true;
      this.successful = false;
      this.errorMsg = error.error;
    });
  }
}
