export class Form {

  constructor(
    public fullName: string,
    public idNumber: string,
    public telephoneNumber: string
  ) {  }

}
