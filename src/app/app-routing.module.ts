import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllFormsComponent } from './all-forms/all-forms.component';
import { FormEntryComponent } from './form-entry/form-entry.component';


const routes: Routes = [
  { path: '', redirectTo: 'form-entry', pathMatch: 'full'},
  { path: 'form-entry', component: FormEntryComponent },
  { path: 'all-forms', component: AllFormsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
